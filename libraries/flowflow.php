<?php

class flowflow {

    public function __construct( $init = null )
    {
        if( $init ){
            $this->index($init);
        }
        else {
            $this->_404();
        }
    }

    public function index($param)
    {
        echo 'hey there ' . $param . ', welcome to flowflow';
        exit;
    }

    public function _404()
    {
        echo 'who even is she?';
        exit;
    }
}
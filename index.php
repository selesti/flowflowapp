<?php

/*  Welcome to FlowFlow
*   The Ultimate Way To Experience
*   The Wonders of Git Flow!
*/

$libraries = array(
    'flowflow',
    'hello-world',
    'example'
);

foreach( $libraries as $l ){
    include( 'libraries/'.$l.'.php' );
}

require_once('core/router.php');